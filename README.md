# gtvi (Graphical Terminal Vi)

A simple wrapper for vi, vim, neovim (nvim), and other vi like tools.

## Usage

	gvti $path $line $colunm

### Why?

I created it to allow konsole to open a text files in another terminal instead of gvim (as it's bloat!)
